let express = require('express');
let expressSession = require('express-session');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let flash = require('connect-flash');
let passport = require('passport');
let LocalStrategy = require('passport-local').Strategy;
let MySQLStore = require('express-mysql-session')(expressSession);
let expressValidator = require('express-validator');
let bcrypt = require('bcrypt');

let index = require('./routes/index');
let users = require('./routes/users');
let reportings = require('./routes/reportings');
let customizations = require('./routes/customizations');
let admins = require('./routes/admins');
let signs = require('./routes/sign');
let projects = require('./routes/projects');
let getAnalytics = require('./routes/getAnalytics');
let models = require('./models');
let config = require('./config/config');

const User = models.User;


let app = express();
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

models.sequelize.sync({force: false});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favico.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(expressValidator());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

if (process.env.NODE_ENV !== 'production') {
    let options = {
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'analytics'
    };

    let sessionStore = new MySQLStore(options);
    app.use(expressSession({
        secret: 'thisIsMySecretThatIsReallySecretSoHushAboutItPlzOrItWillNotBeASecretAnymore',
        store: sessionStore,
        resave: false,
        saveUninitialized: false
    }));
} else {
    let options = {
        host: config.production.host,
        user: config.production.username,
        password: config.production.password,
        database: config.production.database
    };
    let sessionStore = new MySQLStore(options);
    app.use(expressSession({
        secret: 'thisIsMySecretThatIsReallySecretSoHushAboutItPlzOrItWillNotBeASecretAnymore',
        store: sessionStore,
        resave: false,
        saveUninitialized: false
    }));
}


app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use(function (req, res, next) {
    res.locals.isAuthenticated = req.isAuthenticated();
    next();
});

app.all('/admin/*', requireRole('admin'));
app.all('/admin', requireRole('admin'));

function requireRole(role) {
    return function (req, res, next) {
        if (req.session.passport.user && req.session.passport.user.accountlevel === role) {
            next();
        } else {
            res.sendStatus(403);
        }
    }
}

app.use('/', index);
app.use('/users', users);
app.use('/reporting', reportings);
app.use('/customization', customizations);
app.use('/admin', admins);
app.use('/sign', signs);
app.use('/projects', projects);
app.use('/getanalytics', getAnalytics);

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, function (email, password, done) {
    User.find({
        where: {
            email: email
        }
    }).then(user => {
        if (user) {
            const hash = user.password;

            bcrypt.compare(password, hash, function (err, response) {
                if (response === true) {
                    return done(null, {
                        user_id: user.id,
                        accountlevel: user.accountlevel,
                        lastname: user.lastname,
                        firstname: user.firstname,
                        email: user.email
                    });
                } else {
                    return done(null, false);
                }
            });
        } else {
            return done(null, false);
        }
    });
}));


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// error handler
app.use(function (err, req, res) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
