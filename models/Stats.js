module.exports =  (sequelize, DataTypes) => {
    const Stats = sequelize.define('Stats', {
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        baseurl: DataTypes.STRING,
        route: DataTypes.STRING,
        userip: DataTypes.STRING,
        eventname: DataTypes.STRING,
        category: DataTypes.STRING,
        action: DataTypes.STRING,
    }, {
        freezeTableName: true,
        paranoid: true
    });
    return Stats;
};