'use strict';
module.exports =  (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        accountlevel: {
            type: DataTypes.STRING,
            defaultValue: 'user',
        },
        lastname: {
            type: DataTypes.STRING,
        },
        firstname: {
            type: DataTypes.STRING,
        },
        email: {
            type: DataTypes.STRING,
            unique: true
        },
        password: DataTypes.STRING,
        confirmed: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        }
    }, {
        freezeTableName: true,
        paranoid: true
    });

    User.associate = function (models) {
        User.belongsToMany(models.Project, {
            foreignKey: 'user',
            through: 'ProjectUser'
        });
        User.hasMany(models.Project, {
            foreignKey: 'owner',
        });
    };

    return User;
};