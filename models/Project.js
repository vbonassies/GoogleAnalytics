module.exports = (sequelize, DataTypes) => {
    const Project = sequelize.define('Project', {
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        name: DataTypes.STRING,
        urlfollowed: DataTypes.STRING
    }, {
        freezeTableName: true,
        paranoid: true
    });

    Project.associate = function (models) {
        Project.belongsToMany(models.User, {
            through: 'ProjectUser',
            foreignKey: 'project'
        });
        Project.belongsTo(models.User, {
            foreignKey: 'owner'
        });
    };

    return Project;
};