module.exports =  (sequelize, DataTypes) => {
    const ProjectUser = sequelize.define('ProjectUser', {
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
        paranoid: true
    });

    ProjectUser.associate = function (models) {
        ProjectUser.belongsTo(models.Project, {foreignKey: 'project'});
        ProjectUser.belongsTo(models.User, {foreignKey: 'user'});
    };

    return ProjectUser;
};