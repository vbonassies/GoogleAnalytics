'use strict';
let mysql = require('mysql');
let host = require('./config.json').production.host;
let username = require('./config.json').production.username;
let pass = require('./config.json').production.password;
let database = require('./config.json').production.database;

if(process.env.NODE_ENV !== 'production') {
    let connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'analytics'
    });
    connection.connect();

    module.exports = connection;
}else {
    let connection = mysql.createConnection({
        host: host,
        user: username,
        password: pass,
        database: database

    });
    connection.connect();

    module.exports = connection;
}


