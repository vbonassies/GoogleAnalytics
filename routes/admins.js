'use strict';
let express = require('express');
let router = express.Router();
let nodemailer = require('nodemailer');
const models = require('../models');
const User = models.User;


router.get('/', function (req, res, next) {
    res.render('admin', {title: 'Admin', r: req.session.passport});
});

router.get('/createuser', function (req, res, next) {
    res.render('createuser', {title: 'Create a User Account', r: req.session.passport});
});

router.post('/createuser', function (req, res) {
    req.checkBody('email', 'The email you entered is invalid, please try again.').isEmail();
    req.checkBody('email', 'Email address must be between 4-100 characters long, please try again.').len(4, 100);

    const mail = req.body.email;
    const lname = req.body.lastname;
    const fname = req.body.firstname;

    let transporter = require('../config/mailer');
    let hostname = req.hostname + ':' + req.connection.localPort;
    let link = 'http://' + hostname + '/sign/signbyinvite?email=' + mail + '&lastname=' + lname + '&firstname=' + fname;

    let mailOptions = {
        from: transporter.options.auth.user,
        to: mail,
        subject: 'Invitation to join project_analytics',
        html: 'Hey,<br> Please click on the link below to register into project_analytics website.<br>' +
        '<a href=' + link + '>Click Here</>'
    };

    transporter.sendMail(mailOptions, function (err, info) {
        if (err) {
            throw err;
        }
        res.render('index', {message: 'email sent', r: req.session.passport});
    })
});

router.get('/addadmin', function (req, res) {
    res.render('addadmin', {title: 'Add a new Admin', r: req.session.passport});
});

router.post('/addadmin', function (req, res) {
    let email = req.body.email;
    User.find({
        where: {
            email: email
        }
    }).then(user => {
        if (user.accountlevel === 'admin') {
            res.render('addadmin', {
                title: 'User is already an admin',
                r: req.session.passport
            });
        }
        if (user != null) {
            user.update({accountlevel: 'admin'}).then(ok => {
                res.render('addadmin',
                    {
                        title: 'Admin added',
                        r: req.session.passport
                    });
            }).catch(err => {
                res.render('addadmin',
                    {
                        title: 'Something went wrong',
                        errors: err
                    });
            });
        } else {
            res.render('addadmin',
                {
                    title: 'User not found',
                    r: req.session.passport
                });
        }
    }).catch(err => {
        res.render('addadmin',
            {
                title: 'User not found',
                errors: err
                , r: req.session.passport
            });
    });
});
module.exports = router;