let express = require('express');
let router = express.Router();
const utils = require('./utils');
const Op = require('sequelize').Op;
const models = require('../models');
const Project = models.Project;
const Stats = models.Stats;
const User = models.User;


/* GET home page. */
router.get('/', function (req, res, next) {
    utils.sendAnalytics(req.host, req.originalUrl, req.ip, 'index', 'page', 'get');
    if (req.isAuthenticated()) {
        User.find({
            where: {id: {[Op.eq]: parseInt(req.session.passport.user.user_id)}}
        }).then(user => {
            return Project.findAll({
                where: {owner: {[Op.eq]: user.id}}
            }).then(projects => {
                if (projects.length) {
                    user.Projects = projects;
                    let url = projects[0].urlfollowed;
                    return Stats.findAll({
                        where: {baseurl: {[Op.eq]: url}},
                        order: [['createdAt', 'DESC']]
                    }).then(stats => {
                        projects.stats = stats;
                        let testArr = [];
                        let track = 'eventname';
                        projects.track = track;
                        for (let c of stats) {
                            testArr.push(c[track]);
                        }
                        let charArr = utils.countOccurrence(testArr);
                        charArr = utils.processDataForGraphs(charArr);

                        res.render('index', {
                            title: 'Project Analytics',
                            r: req.session.passport,
                            user: user,
                            project: projects,
                            chartArray: charArr
                        });
                    });
                } else {
                    res.render('index', {
                        title: 'Project analytics',
                        r: req.session.passport,
                        user: user
                    });
                }
            })
        });
    } else {
        res.render('index', {
            title: 'Project analytics',
            r: req.session.passport
        });
    }
});

module.exports = router;
