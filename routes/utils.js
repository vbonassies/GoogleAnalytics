'use strict';

const models = require('../models');
const Stats = models.Stats;

module.exports = {
    sendAnalytics: function (baseUrl, route, ip, eventName, category, action) {
        Stats.create({
            baseurl: baseUrl,
            route: route,
            userip: ip,
            eventname: eventName,
            category: category,
            action: action
        }).then(s => {
            console.log('STAT CREATED =>' + s.action);
        });
    },

    countOccurrence: function (arr) {
        let a = [], b = [], prev = '------------';

        let bl = [];
        for (let a of arr) {
            if(!Number.isInteger(a)) {
                bl.push(a.toLowerCase());
            } else {
                bl.push(a);
            }
        }
        arr = bl;

        arr.sort();
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] !== prev) {
                a.push(arr[i]);
                b.push(1);
            } else {
                b[b.length - 1]++;
            }
            prev = arr[i];
        }
        return [a, b];
    },

    processDataForGraphs: function(data) {
        let s1 = '[';
        for (let v of data[0]) {
            s1 = s1 + "'" + v + "',"
        }
        s1 = s1 + ']';
        data[0] = s1;
        return data;
    },

    format: function(d) {
        let newD = d.split(" ");
        return newD[0] + ' ' + newD[1] + ' ' + newD[2] + ' ' + newD[3];
    }
};