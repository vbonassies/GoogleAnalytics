'use strict';
let express = require('express');
let router = express.Router();

const utils = require('./utils');

function checkParams(d) {
    return d.baseUrl && d.route && d.ip && d.eventName && d.category && d.action;
}

router.post('/', function (req, res) {
    let params = {
        baseUrl: req.hostname,
        route: '/' + req.headers.referer.split("/")[3],
        ip: req.ip,
        eventName: req.body.eventName,
        category: req.body.category,
        action: req.body.action
    };
    console.log(JSON.stringify(params));
    if (checkParams(params)) {
        utils.sendAnalytics(params.baseUrl, params.route, params.ip, params.eventName, params.category, params.action);
        console.log({message: 'error'});
        res.json({message: 'OK'});
    } else {
        console.log({message: 'error'});
        res.json({message: 'error'});
    }
});

module.exports = router;