'use strict';
let express = require('express');
let router = express.Router();
const Op = require('sequelize').Op;
const models = require('../models');
const Project = models.Project;
const ProjectUser = models.ProjectUser;
const Stats = models.Stats;
const User = models.User;
const utils = require('./utils');

/* GET reports listing. */
router.get('/', function (req, res, next) {
    utils.sendAnalytics(req.host, req.originalUrl, req.ip, 'reporting', 'page', 'get');

    User.find({
        where: {id: {[Op.eq]: parseInt(req.session.passport.user.user_id)}}
    }).then(user => {
        return Project.findAll({
            where: {owner: {[Op.eq]: user.id}}
        }).then(projects => {
            user.Projects = projects;
            return ProjectUser.findAll({
                include: [{model: Project}],
                where: {
                    user: {
                        [Op.eq]: user.id
                    }
                }
            }).then(pus => {
                let listID = [];
                for (let pu of pus) listID.push(pu.project);
                // user.ProjectUsers = pus;
                return Project.findAll({
                    where: {id: {[Op.in]: listID}}
                }).then(participations => {
                    let listID = [];
                    for (let pu of participations) listID.push(pu.id);
                    user.Participations = participations;
                    res.render('reporting', {
                        title: 'Reportings',
                        r: req.session.passport,
                        user: user
                    });
                })

            })
        })
    });
});

router.post('/getprojectforandroid', function (req, res) {
    User.find({
        where: {id: req.body.id}
    }).then(user => {
        if(!user) {
            res.status(404).json({error: 'User not found'});
        } else {
            return Project.findAll({
                where: {owner: {[Op.eq]: user.id}}
            }).then(projects => {
                if (!projects) {
                    res.status(404).json({error: 'Something went wrong with the projects'});
                } else {
                    user.Projects = projects;
                    return ProjectUser.findAll({
                        include: [{model: Project}],
                        where: {
                            user: {
                                [Op.eq]: user.id
                            }
                        }
                    }).then(pus => {
                        if (!pus) {
                            res.status(404).json({error: 'Something went wrong with the collaborations'});
                        } else {
                            let listID = [];
                            for (let pu of pus) listID.push(pu.project);
                            return Project.findAll({
                                where: {id: {[Op.in]: listID}}
                            }).then(participations => {
                                if (!participations) {
                                    res.status(404).json({error: 'Something went wrong with the participations'});
                                } else {
                                    let listID = [];
                                    for (let pu of participations) listID.push(pu.id);
                                    user.Participations = participations;
                                    res.send({
                                        projectOwn: user.Projects,
                                        collaborations: user.Participations
                                    });
                                }
                            })

                        }
                    })
                }
            })
        }});
});

router.get('/createproject', function (req, res) {
    utils.sendAnalytics(req.host, req.originalUrl, req.ip, 'createProject', 'page', 'get');

    res.render('createproject', {title: 'CreateProject', r: req.session.passport});
});

router.get('/addparticipant', function (req, res) {
    utils.sendAnalytics(req.host, req.originalUrl, req.ip, 'addParticipant', 'page', 'get');

    res.render('addparticipant', {title: 'Add a collaborator', r: req.session.passport});
});

router.post('/createproject', function (req, res) {
    utils.sendAnalytics(req.host, req.originalUrl, req.ip, 'CreateProject', 'button', 'post');

    let name = req.body.projectname;
    let url = req.body.urlfollowed;
    let owner = req.session.passport.user.user_id;
    Project.create({name: name, urlfollowed: url, owner: owner}).then(() => {
        res.redirect('/reporting');
    }).catch(err => {
        res.json({msg: 'A', e: err});
    });
});

module.exports = router;