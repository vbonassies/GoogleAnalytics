'use strict';
let express = require('express');
let router = express.Router();
const Op = require('sequelize').Op;
const models = require('../models');
const Project = models.Project;
const Stats = models.Stats;
const User = models.User;
const ProjectUser = models.ProjectUser;

const utils = require('./utils');


router.post('/project/getstatforandroid', function(req, res) {
    Project.find({
        include: [{model: User, as: 'Users'}],
        where: {id: {[Op.eq] :  parseInt(req.body.projectID)}}
    }).then(project => {
        let url = project.urlfollowed;
        return Stats.findAll({where: {baseurl: {[Op.eq]: url}}, order: [['createdAt', 'DESC']]}).then(stats => {
            if(!stats) res.status(404).json({error: 'Stat not found'});
            res.send(stats);
        }).catch(() => {res.status(404).send({error: 'Oh snap! Something went wrong...'});});
    }).catch(() => {res.status(404).send({error: 'Sacrebleu! Something went wrong...'});});
});

router.post('/project/getspecificstatforandroid', function(req, res) {
    Project.find({
        include: [{model: User, as: 'Users'}],
        where: {id: {[Op.eq] :  parseInt(req.body.projectID)}}
    }).then(project => {
        let url = project.urlfollowed;
        let event = req.body.eventtype;
        let ename = req.body.name;
        let filter;
        if (event === 'route') filter = {baseurl: {[Op.eq]: url}, route: {[Op.eq]: ename}};
        else if (event === 'userip') filter = {baseurl: {[Op.eq]: url}, userip: {[Op.eq]: ename}};
        else if (event === 'eventname') filter = {baseurl: {[Op.eq]: url}, eventname: {[Op.eq]: ename}};
        else if (event === 'category') filter = {baseurl: {[Op.eq]: url}, category: {[Op.eq]: ename}};
        else if (event === 'action') filter = {baseurl: {[Op.eq]: url}, action: {[Op.eq]: ename}};
        else filter = {baseurl: {[Op.eq]: url}};
        return Stats.findAll({
            where: filter,
            order: [['createdAt', 'DESC']]
        }).then(stats => {
            if (!stats) res.status(404).json({error: 'Stat not found'});
            if(stats.length === 0) res.status(404).json({error: 'Parameters are probably wrong'});
            res.send(stats);
        }).catch(() => {
            res.status(404).send({error: 'Oh snap! Something went wrong...'});
        });
    }).catch(() => {res.status(404).send({error: 'Sacrebleu! Something went wrong...'});});
});

router.get('/project/trackspecificstat/:projectID/:eventtype/:name/:date', function(req, res) {
    Project.find({
        include: [{model: User, as: 'Users'}],
        where: {id: {[Op.eq] :  parseInt(req.params.projectID)}}
    }).then(project => {
        let url = project.urlfollowed;
        let event = req.params.eventtype;
        let ename = req.params.name;
        let time = req.params.date;
        let filter;
        if (event === 'route') filter = {baseurl: {[Op.eq]: url}, route: {[Op.eq]: ename}};
        else if (event === 'userip') filter = {baseurl: {[Op.eq]: url}, userip: {[Op.eq]: ename}};
        else if (event === 'eventname') filter = {baseurl: {[Op.eq]: url}, eventname: {[Op.eq]: ename}};
        else if (event === 'category') filter = {baseurl: {[Op.eq]: url}, category: {[Op.eq]: ename}};
        else if (event === 'action') filter = {baseurl: {[Op.eq]: url}, action: {[Op.eq]: ename}};
        else filter = {baseurl: {[Op.eq]: url}};
        return Stats.findAll({
            where: filter,
            order: [['createdAt', 'DESC']]
        }).then(stats => {
            if (!stats) res.status(404).send({error: 'Stat not found'});
            if(stats.length === 0) res.status(404).send({error: 'Parameters are probably wrong'});
            let toStats = [];
            if(time === "month") {
                let month = new Date().getMonth();
                let year = new Date().getFullYear();
                for (let s of stats) {
                    let statMonth = s.createdAt.getMonth();
                    let statYear = s.createdAt.getFullYear();
                    if (month === statMonth && year === statYear) {
                        toStats.push(utils.format(s.createdAt.toString()));
                    }
                }
            }
            if(time === "day") {
                let month = new Date().getMonth();
                let year = new Date().getFullYear();
                let day = new Date().getDate();
                for (let s of stats) {
                    let statDay = s.createdAt.getDate();
                    let statMonth = s.createdAt.getMonth();
                    let statYear = s.createdAt.getFullYear();
                    if (day === statDay && month === statMonth && year === statYear) {
                        toStats.push(utils.format(s.createdAt.toString()));
                    }
                }
            }
            if(time === "year") {
                let year = new Date().getFullYear();
                for (let s of stats) {
                    let statYear = s.createdAt.getFullYear();
                    if (year === statYear) {
                        toStats.push(utils.format(s.createdAt.toString()));
                    }
                }
            }
            project.stats = toStats;
            let charArr = utils.countOccurrence(toStats);
            charArr = utils.processDataForGraphs(charArr);
            project.by = 'month';
            res.render('trackstatbytime', {
                title: 'Stat by time',
                r: req.session.passport,
                project: project,
                chartArray: charArr,
                catName: ename,
                catDate: time
            });
        }).catch(err => {
            res.render('trackstatbytime', {
                title: err,
            r: req.session.passport,
            project: project});
        });
    }).catch(err => {
        res.render('trackstatbytime', {
            title: err,
            r: req.session.passport,
            project: project});
    });
});

router.get('/project/statsordered/:projectID/:by/:track', function (req, res) {
    Project.find({
        include: [{model: User, as: 'Users'}],
        where: {id: {[Op.eq]: parseInt(req.params.projectID)}}
    }).then(project => {
        let url = project.urlfollowed;
        let by = req.params.by;
        if (by === 'lasts') {
            return Stats.findAll({where: {baseurl: {[Op.eq]: url}}, order: [['createdAt', 'DESC']]}).then(stats => {
                project.stats = stats;
                let testArr = [];
                let track = req.params.track;
                project.track = track;
                for (let c of stats) {
                    testArr.push(c[track]);
                }
                let charArr = utils.countOccurrence(testArr);
                charArr = utils.processDataForGraphs(charArr);
                project.by = 'lasts';
                res.render('printstatsordered', {
                    title: 'Reportings',
                    r: req.session.passport,
                    project: project,
                    chartArray: charArr
                });
            });
        } else if (by === 'today') {
            let day = new Date().getDate();
            let month = new Date().getMonth();
            let year = new Date().getFullYear();
            return Stats.findAll({where: {baseurl: {[Op.eq]: url}}, order: [['createdAt', 'DESC']]}).then(stats => {
                let toStats = [];
                for (let s of stats) {
                    let statDay = s.createdAt.getDate();
                    let statMonth = s.createdAt.getMonth();
                    let statYear = s.createdAt.getFullYear();
                    if (day === statDay && month === statMonth && year === statYear) toStats.push(s);
                }
                project.stats = toStats;
                let testArr = [];
                let track = req.params.track;
                project.track = track;
                for (let c of toStats) {
                    testArr.push(c[track]);
                }
                let charArr = utils.countOccurrence(testArr);
                charArr = utils.processDataForGraphs(charArr);
                project.by = 'today';
                res.render('printstatsordered', {
                    title: 'Reportings',
                    r: req.session.passport,
                    project: project,
                    chartArray: charArr
                });
            });
        } else if (by === 'week') {
            let today = Math.round((new Date().getTime() / ((1000 * 60 * 60 * 24 * 7))));
            return Stats.findAll({where: {baseurl: {[Op.eq]: url}}, order: [['createdAt', 'DESC']]}).then(stats => {
                let toStats = [];
                for (let s of stats) {
                    let statToday = Math.round((s.createdAt.getTime() / ((1000 * 60 * 60 * 24 * 7))));
                    if (today === statToday) toStats.push(s);
                }
                project.stats = toStats;
                let testArr = [];
                let track = req.params.track;
                project.track = track;
                for (let c of toStats) {
                    testArr.push(c[track]);
                }
                let charArr = utils.countOccurrence(testArr);
                charArr = utils.processDataForGraphs(charArr);
                project.by = 'week';
                res.render('printstatsordered', {
                    title: 'Reportings',
                    r: req.session.passport,
                    project: project,
                    chartArray: charArr
                });
            });
        } else if (by === 'month') {
            let month = new Date().getMonth();
            let year = new Date().getFullYear();
            return Stats.findAll({where: {baseurl: {[Op.eq]: url}}, order: [['createdAt', 'DESC']]}).then(stats => {
                let toStats = [];
                for (let s of stats) {
                    let statMonth = s.createdAt.getMonth();
                    let statYear = s.createdAt.getFullYear();
                    if (month === statMonth && year === statYear) toStats.push(s);
                }
                project.stats = toStats;
                let testArr = [];
                let track = req.params.track;
                project.track = track;
                for (let c of toStats) {
                    testArr.push(c[track]);
                }
                let charArr = utils.countOccurrence(testArr);
                charArr = utils.processDataForGraphs(charArr);
                project.by = 'month';
                res.render('printstatsordered', {
                    title: 'Reportings',
                    r: req.session.passport,
                    project: project,
                    chartArray: charArr
                });
            });
        } else {
            project.by = 'lasts';
            res.render('printstatsordered', {
                title: 'Reportings',
                r: req.session.passport,
                project: project,
            });
        }
    });
});

router.get('/project/:projectID', function (req, res) {
    utils.sendAnalytics(req.host, req.originalUrl, req.ip, 'Project', 'page', 'get');
    Project.find({
        include: [{model: User, as: 'Users'}],
        where: {id: {[Op.eq]: parseInt(req.params.projectID)}}
    }).then(project => {
        res.render('project', {
            title: 'Reportings',
            r: req.session.passport,
            project: project,
        });
    });
});

router.get('/addcollaborator/:projectID', function (req, res) {
    utils.sendAnalytics(req.host, req.originalUrl, req.ip, 'AddCollaborator', 'page', 'get');
    Project.find({
        include: [{model: User, as: 'Users'}],
        where: {id: {[Op.eq]: parseInt(req.params.projectID)}}
    }).then(project => {
        res.render('addcollaborator', {
            title: 'Add a collaborator to your project',
            r: req.session.passport,
            project: project
        });
    });
});

router.post('/addcollaborator', function (req, res) {
    utils.sendAnalytics(req.host, req.originalUrl, req.ip, 'AddCollaborator', 'button', 'post');

    let email = req.body.email;
    let proID = req.body.projectid;
    Project.find({
        include: [{model: User, as: 'Users'}],
        where: {id: {[Op.eq]: parseInt(req.body.projectid)}}
    }).then(project => {
        User.find({
            where: {
                email: email
            }
        }).then(user => {
            if (user != null) {
                return ProjectUser.create({
                    project: project.id,
                    user: user.id
                }).then(bla => {
                    res.redirect('/projects/project/' + project.id);
                }).catch(err => {
                    res.render('addcollaborator',
                        {
                            projectID: proID,
                            title: 'An error occur',
                            errors: err
                        });
                });
            } else {
                res.render('addcollaborator',
                    {
                        title: 'User not found',
                        projectID: proID,
                    });
            }
        }).catch(err => {
            res.render('addcollaborator',
                {
                    title: 'User not found',
                    projectID: proID,
                    errors: err
                });
        });
    });
});

module.exports = router;