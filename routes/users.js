'use strict';
let express = require('express');
let router = express.Router();
const Op = require('sequelize').Op;
const fs = require('fs');

const models = require('../models');
const User = models.User;
const Project = models.Project;
const ProjectUser = models.ProjectUser;
const Stats = models.Stats;
const utils = require('./utils');


router.get('/', function (req, res, next) {
    utils.sendAnalytics(req.host, req.originalUrl, req.ip, 'user', 'page', 'get');

    User.find({
        include: [{model: Project, include: [{model: Stats}]}],
        where: {
            id: {
                [Op.eq]: parseInt(req.session.userid)
            }
        }
    }).then(user => {
        res.send('respond with a resource');
    })
});

router.post('/update-user', function (req, res) {
    User.find({
        where: {
            id: {
                [Op.eq]: parseInt(req.body.userid)
            }
        }
    }).then(user => {
        return user.update().then(() => {
            res.redirect('/users');
        })
    })
});

router.get('/deleteData', function (req, res) {
    User.find({where: {id: {[Op.eq]: req.session.passport.user.user_id}}}).then(user => {
        return Project.findAll({
            where: {owner: {[Op.eq]: user.id}}
        }).then(projectsWhereOwner => {
            for (let project of projectsWhereOwner) {
                project.destroy();
            }
            return ProjectUser.findAll({
                include: [{model: Project, where: {owner: {[Op.ne]: user.id}}}],
                where: {user: {[Op.eq]: user.id}}
            }).then(projectUsers => {
                for (let project of projectUsers) {
                    project.destroy();
                }
                return user.destroy().then(() => {
                    res.redirect('/sign/logout');
                })
            })
        })
    });
});

router.get('/downloadData', function (req, res) {
    User.find({where: {id: {[Op.eq]: req.session.passport.user.user_id}}}).then(user => {
        return Project.findAll({
            where: {owner: {[Op.eq]: user.id}}
        }).then(projectsWhereOwner => {
            let stringToWrite ="";
            for(let pro of projectsWhereOwner) {
                stringToWrite += "name: " +pro.name + " | urlfollowed: " + pro.urlfollowed + " | ";
            }
            return ProjectUser.findAll({
                include: [{model: Project, where: {owner: {[Op.ne]: user.id}}}],
                where: {user: {[Op.eq]: user.id}}
            }).then(projectUsers => {
                let list = [];
                for (let proU of projectUsers) { list.push(proU.id); }
                Project.findAll({
                    where: {id: {[Op.in]: list}}
                }).then(projectsCollab => {
                    for (let pr of projectsCollab) {
                        stringToWrite += "project where you collaborated: " + pr.name +
                                        " | urlfollowed: " + pr.urlfollowed + " | ";
                    }
                    fs.writeFile("projectAnalyticsUserData.txt", stringToWrite, function (err) {
                        if (err) {
                            return console.log(err);
                        }
                        console.log("The file was saved!");
                    });
                    res.redirect('/users/download');
                })
            })
        })
    });
});

router.get('/download', function (req, res) {
    let file = "projectAnalyticsUserData.txt";
    res.download(file, function (err) {
        if(err) {
            console.log("err: " + err);
        }else {
            fs.unlink("projectAnalyticsUserData.txt", function (erro) {
                if(erro) {
                    console.log(erro);
                } else {
                    console.log('file deleted');
                }
            });
        }
    });
});

module.exports = router;
