let express = require('express');
let router = express.Router();

router.get('/', function(req, res, next) {
    res.render('customization', { title: 'Customization', r: req.session.passport  });
});

module.exports = router;