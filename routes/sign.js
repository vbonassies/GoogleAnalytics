'use strict';
let express = require('express');
let jwt = require('jsonwebtoken');
let router = express.Router();
const passport = require('passport');
let bcrypt = require('bcrypt');
let transporter = require('../config/mailer');
let url = require('url');
const utils = require('./utils');

const models = require('../models');
const User = models.User;

let mailOptions;
const saltRounds = 10;

router.post('/loginfromandroid', function (req, res) {
    User.find({where: {email: req.body.email}}).then(user => {
        if (!user) {
            res.status(404).json({error: 'User not found'});
        }else { bcrypt.compare(req.body.password, user.password, function (err, result) {
            if (result) {
                res.send(user);
            } else {
                res.status(404).send({error: 'Wrong email or password or both...'});
            }
        });}
    }).catch(() => {res.status(404).send({error: 'Something went wrong...'});});
});

router.get('/profile', authentificationMiddleware(), function (req, res) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'profile', 'page', 'get');

    res.render('profile', {title: 'Profile', r: req.session.passport});
});

router.get('/login', function (req, res) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'login', 'page', 'get');

    res.render('login', {title: 'Login Page'});
});

router.post('/login', function (req, res, next) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'login', 'button', 'post');

    User.find({
        where: {
            email: req.body.email
        }
    }).then(user => {
        if (user) {
            if (user.confirmed) {
                next();
            } else {
                res.render("login", {message: 'Please check your mail to activate your account'});
            }
        } else {
            res.render("login", {message: 'Account not found'});
        }
    }).catch(err => {
        throw err;
    });
}, passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/sign/login'
}));

router.get('/signup', function (req, res) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'signUp', 'page', 'get');

    res.render('signin', {title: 'Signin Page', message: 'Welcome to the signin Page'});
});

router.post('/signupfromandroid', function (req, res) {
    req.checkBody('email', 'The email you entered is invalid, please try again.').isEmail();
    req.checkBody('email', 'Email address must be between 4-100 characters long, please try again.').len(4, 100);
    req.checkBody('password', 'Password must be between 8-100 characters long.').len(8, 100);
    req.checkBody("password", "Password must include one lowercase character," +
        " one uppercase character, a number, and a special character.")
        .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.* )(?=.*[^a-zA-Z0-9]).{8,}$/, "i");
    req.checkBody('password2', 'Password must be between 8-100 characters long.').len(8, 100);
    req.checkBody('password2', 'Passwords do not match, please try again.').equals(req.body.password);

    const errors = req.validationErrors();
    if (errors) {
        res.status(404).send({error: errors});

    } else {
        const fname = req.body.firstname;
        const lname = req.body.lastname;
        const mail = req.body.email;
        const pass = req.body.password;

        User.findAll({
            where: {email: mail}
        }).then(u => {
            if(u.length) {
                res.status(404).send({msg: "Email already used"});
            } else {
                bcrypt.hash(pass, saltRounds, function (err, hash) {
                    const token = jwt.sign({mail: mail}, 'blblblblblbl');
                    let hostname = req.hostname + ':' + req.connection.localPort;
                    let link = 'http://' + hostname + '/sign/verify?token=' + token;

                    mailOptions = {
                        to: mail,
                        subject: 'Please confirm your Email account',
                        html: 'Hello, <br> Please Click on the link to verify your email.' +
                        '<br><a href=' + link + '>Click here</a>'
                    };

                    transporter.sendMail(mailOptions, function (err) {
                        if (err) {
                            res.status(404).send({error: err});
                        }else {
                            User.create({firstname: fname, lastname: lname, email: mail, password: hash});
                            res.status(200).send({msg: "Check your mails to activate your account"});
                        }
                    });
                });
            }
        });
    }
});

router.post('/signup', function (req, result) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'signUp', 'button', 'post');

    req.checkBody('email', 'The email you entered is invalid, please try again.').isEmail();
    req.checkBody('email', 'Email address must be between 4-100 characters long, please try again.').len(4, 100);
    req.checkBody('password', 'Password must be between 8-100 characters long.').len(8, 100);
    req.checkBody("password", "Password must include one lowercase character," +
        " one uppercase character, a number, and a special character.")
        .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.* )(?=.*[^a-zA-Z0-9]).{8,}$/, "i");
    req.checkBody('password2', 'Password must be between 8-100 characters long.').len(8, 100);
    req.checkBody('password2', 'Passwords do not match, please try again.').equals(req.body.password);

    const errors = req.validationErrors();
    if (errors) {
        console.log(`errors:  ${JSON.stringify(errors)}`);
        result.render('signin',
            {
                title: 'Registration Error',
                errors: errors
            });
    } else {
        const fname = req.body.firstname;
        const lname = req.body.lastname;
        const mail = req.body.email;
        const pass = req.body.password;

        User.findAll({
            where: {email: mail}
        }).then(u => {
            if(u.length) {
                result.render('signin',
                    {
                        title: 'Email already used',
                    });
            } else {
                bcrypt.hash(pass, saltRounds, function (err, hash) {
                    const token = jwt.sign({mail: mail}, 'blblblblblbl');
                    let hostname = req.hostname + ':' + req.connection.localPort;
                    let link = 'http://' + hostname + '/sign/verify?token=' + token;

                    mailOptions = {
                        to: mail,
                        subject: 'Please confirm your Email account',
                        html: 'Hello, <br> Please Click on the link to verify your email.' +
                        '<br><a href=' + link + '>Click here</a>'
                    };

                    transporter.sendMail(mailOptions, function (err) {
                        if (err) {
                            console.log('err in the mail sending');
                            throw err;
                        } else {
                            console.log('mail sent successfully');
                            User.create({firstname: fname, lastname: lname, email: mail, password: hash});
                            result.render('index', {message: 'Please check your mail to activate your account'});
                        }
                    });
                });
            }
        })
    }
});

passport.serializeUser(function (user_id, done) {
    done(null, user_id);
});

passport.deserializeUser(function (user_id, done) {
    done(null, user_id);
});

function authentificationMiddleware() {
    return (req, res, next) => {
        if (req.isAuthenticated()) return next();
        res.render('login', {title: 'Please login to view your profile page'});
    }
}

router.get('/logout', function (req, res) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'logout', 'page', 'get');

    req.logout();
    req.session.destroy();
    res.redirect('/');
});

router.get('/verify', function (req, res) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'verify', 'page', 'get');

    let queryData = url.parse(req.url, true).query;
    let token = queryData.token;
    const decode = jwt.verify(token, 'blblblblblbl');
    const usermail = decode.mail;
    console.log("usermail: " + usermail);
    let hostname = req.hostname + ':' + req.connection.localPort;
    if ((req.protocol + '://' + req.get('host')) === ('http://' + hostname)) {
        console.log('Domain is matched. Information is from Authentic email');

        User.find({where: {email: usermail}}).then(user => {
            if (user) {
                if (user.confirmed) {
                    res.render('verify', {r: req.session.passport, message: 'Bad request'});
                } else {
                    return user.update({confirmed: true}, {fields: ['confirmed']}).then(() => {
                        res.render('index', {
                            r: req.session.passport,
                            message: 'Your email had been successfully verified. You can now login'
                        });
                    })
                }
            }
        }).catch(err => {
            throw err;
        });
    } else {
        res.render('verify', {r: req.session.passport, message: 'unknown source'});
    }
});

router.get('/signbyinvite', function (req, res) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'signByInvite', 'page', 'get');

    let queryData = url.parse(req.url, true).query;
    let mail = queryData.email;
    if (mail) mail = mail.replace(' ', '+');
    let lname = queryData.lastname;
    let fname = queryData.firstname;
    res.render('signbyinvite', {
        message: 'Congrats on your invite, please register by configuring your password',
        mail: mail, lname: lname, fname: fname
    });
});

router.post('/signbyinvite', function (req, result) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'signByInvite', 'button', 'post');

    req.checkBody('password', 'Password must be between 8-100 characters long.').len(8, 100);
    req.checkBody("password", "Password must include one lowercase character," +
        " one uppercase character, a number, and a special character.")
        .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.* )(?=.*[^a-zA-Z0-9]).{8,}$/, "i");
    req.checkBody('password2', 'Password must be between 8-100 characters long.').len(8, 100);
    req.checkBody('password2', 'Passwords do not match, please try again.').equals(req.body.password);

    const errors = req.validationErrors();
    if (errors) {
        console.log(`errors:  ${JSON.stringify(errors)}`);
        result.render(req.url,
            {
                title: 'Registration Error',
                errors: errors
            });
    } else {
        let mail = req.body.mail;
        if (mail) mail = mail.replace(' ', '+');
        let lname = req.body.lname;
        let fname = req.body.fname;
        const pass = req.body.password;
        bcrypt.hash(pass, saltRounds, function (err, hash) {
            User.create({
                firstname: fname,
                lastname: lname,
                email: mail,
                password: hash,
                confirmed: true
            }).then(user => {
                result.render('index', {message: 'You can now login'});
            })
        });
    }
});

router.get('/forgotpass', function (req, res) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'forgetPass', 'page', 'get');

    res.render('passreset', {title: 'Did you forgot your password?'});
});

router.post('/forgotpassfromandroid', function (req, res) {
    let mail = req.body.email;
    User.find({
        where: {
            email: mail
        }
    }).then(user => {
        if (user) {
            const token = jwt.sign({mail: mail}, 'passoreseto');
            let hostname = req.hostname + ':' + req.connection.localPort;
            let link = 'http://' + hostname + '/sign/resetpass?token=' + token;
            mailOptions = {
                to: mail,
                subject: 'Please click on the link below to reset your password, if you didn\'t ask for it,' +
                ' please ignore this mail',
                html: 'Hello, <br> Please Click on the link to reset your password.' +
                '<br><a href=' + link + '>Click here</a>'
            };

            transporter.sendMail(mailOptions, function (err) {
                if (err) {
                    res.status(404).send({error: err});
                }else {
                    res.status(200).send({msg: "Check your mails to reset your password"});
                }
            });
        } else {
            res.status(404).send({error: "Mail not found"});
        }
    }).catch(err => {
        res.status(404).send({error: err});
    });
});

router.post('/forgotpass', function (req, res) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'forgotPass', 'button', 'post');

    let mail = req.body.email;
    User.find({
        where: {
            email: mail
        }
    }).then(user => {
        if (user) {
            const token = jwt.sign({mail: mail}, 'passoreseto');
            let hostname = req.hostname + ':' + req.connection.localPort;
            let link = 'http://' + hostname + '/sign/resetpass?token=' + token;
            mailOptions = {
                to: mail,
                subject: 'Please click on the link below to reset your password, if you didn\'t ask for it,' +
                ' please ignore this mail',
                html: 'Hello, <br> Please Click on the link to reset your password.' +
                '<br><a href=' + link + '>Click here</a>'
            };

            transporter.sendMail(mailOptions, function (err) {
                if (err) {
                    console.log('err in the mail sending');
                    throw err;
                }
                console.log('reset mail sent successfully');
            });
            res.render('index', {message: 'Please check your mail to reset your password'});
        } else {
            res.render('passreset', {title: 'Mail incorrect'});
        }
    }).catch(err => {
        throw err;
    });
});

router.get('/resetpass', function (req, res) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'resetPass', 'page', 'get');

    let queryData = url.parse(req.url, true).query;
    let token = queryData.token;
    const decode = jwt.verify(token, 'passoreseto');
    const usermail = decode.mail;

    let hostname = req.hostname + ':' + req.connection.localPort;
    if ((req.protocol + '://' + req.get('host')) !== ('http://' + hostname)) {
        res.render('resetpass', {message: 'unknown source'});
    }
    res.render('resetpass', {message: 'Please create your new password', tok: usermail});
});

router.post('/resetpass', function (req, res) {
    utils.sendAnalytics(req.hostname, req.originalUrl, req.ip, 'resetPass', 'button', 'post');

    req.checkBody('password', 'Password must be between 8-100 characters long.').len(8, 100);
    req.checkBody("password", "Password must include one lowercase character," +
        " one uppercase character, a number, and a special character.")
        .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.* )(?=.*[^a-zA-Z0-9]).{8,}$/, "i");
    req.checkBody('password2', 'Password must be between 8-100 characters long.').len(8, 100);
    req.checkBody('password2', 'Passwords do not match, please try again.').equals(req.body.password);

    const errors = req.validationErrors();
    if (errors) {
        console.log(`errors:  ${JSON.stringify(errors)}`);
        res.render('resetpass',
            {
                title: 'Password Reset Error',
                errors: errors
            });
    } else {

        let usermail = req.body.tok;

        const pass = req.body.password;
        bcrypt.hash(pass, saltRounds, function (err, hash) {
            User.find({where: {email: usermail}}).then(user => {
                return user.update({password: hash}, {fields: ['password']}).then(() => {
                    res.render('login', {
                        r: req.session.passport, message: 'Your password has been successfully ' +
                        'updated. You can now login'
                    });
                })
            })
        })
    }
});

module.exports = router;