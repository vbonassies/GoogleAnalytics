function sendAnalytics(eventName, category, action) {
    return new Promise((resolve, reject)=>{
        let xhttp = new XMLHttpRequest();
        xhttp.open('POST', '/getanalytics', true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        let str = 'eventName=' + eventName + '&category=' + category + '&action=' + action;
        xhttp.onreadystatechange = () =>{
            if(xhttp.readyState === 4 && xhttp.status === 200) {
                return resolve(JSON.parse(xhttp.responseText));
            }
            if(xhttp.readyState === 4) {
                return reject(JSON.parse(xhttp.responseText));
            }
        };
        xhttp.send(str);
    });
}