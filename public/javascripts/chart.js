function makeData(data) {
    data[0] = (data[0].toString()).replace(/,]/, ']');
    data[1] = (data[1].join(',')).split(',');
    data[0] = ((data[0].split('[')[1]).split(']')[0]).split(',');

    return data;
}

function chartBuilder() {
    let data = makeData([[document.getElementById('keys').innerHTML], [document.getElementById('values').innerHTML]]);

    let ctx = document.getElementById("myChart");
    let myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: data[0],
            datasets: [{
                label: 'Event stats',
                data: data[1],
                backgroundColor: "#3e95cd"
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

function iframeSize() {
    let ift = window.parent.document.getElementById('iframe-stats');
    ift.style.height = ift.contentWindow.document.body.scrollHeight + 'px';
}